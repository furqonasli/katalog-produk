import React from 'react';
//import { Text, View, StyleSheet } from 'react-native';
import IonIcon from 'react-native-vector-icons/Ionicons';

import Home from './components/Home';
import Detail from './components/Detail';
import Bestseller from './components/Bestseller';

import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

const Stack = createNativeStackNavigator();
const Tabs = createBottomTabNavigator();

function HomeNavigator() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Utama"
        component={Home}
        options={{
          title: 'Home',
          headerStyle: {backgroundColor: '#F9813A'},
          headerTintColor: '#fff',
        }}
      />
      <Stack.Screen
        name="Detail"
        component={Detail}
        options={{
          title: 'Detail Buku',
          headerStyle: {backgroundColor: '#F9813A'},
          headerTintColor: '#fff',
        }}
      />
    </Stack.Navigator>
  );
}

function BestSellerNavigator() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Best Seller"
        component={Bestseller}
        options={{
          title: 'Best Seller',
          headerStyle: {backgroundColor: '#F9813A'},
          headerTitleAlign: 'center',
          headerTintColor: '#fff',
        }}
      />
    </Stack.Navigator>
  );
}

export default function App() {
  return (
    <NavigationContainer>
      <Tabs.Navigator
        screenOptions={({route}) => ({
          tabBarIcon: ({focused, color, size}) => {
            let iconName;

            if (route.name === 'Home') {
              iconName = focused ? 'home' : 'home-outline';
            } else if (route.name === 'BestSeller') {
              iconName = focused ? 'star' : 'star-outline';
            }

            // You can return any component that you like here!
            return <IonIcon name={iconName} size={size} color={color} />;
          },
          tabBarActiveTintColor: '#F9813A',
          tabBarInactiveTintColor: 'gray',
          headerShown: false,
        })}>
        <Tabs.Screen name="Home" component={HomeNavigator} />
        <Tabs.Screen name="BestSeller" component={BestSellerNavigator} />
      </Tabs.Navigator>
    </NavigationContainer>
  );
}
