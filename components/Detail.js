import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  Button,
  ImageBackground,
  ScrollView,
} from 'react-native';

const Detail = ({route}) => {
  const {buku} = route.params;

  return (
    <View style={{flex: 1}}>
      <View style={{flex: 4}}>
        <ImageBackground source={{uri: buku.image}} style={styles.belakang} />
        <View style={styles.overlay}></View>
        <View style={{flex: 5, paddingTop: 36, alignItems: 'center'}}>
          <Image
            source={{uri: buku.image}}
            style={{flex: 1, width: 150, height: 'auto'}}
          />
        </View>
        <View style={{flex: 2, alignItems: 'center', justifyContent: 'center'}}>
          <Text style={{fontSize: 17, fontWeight: 'bold'}}>{buku.judul}</Text>
          <Text style={{fontSize: 15}}>{buku.penulis}</Text>
        </View>
      </View>
      <View style={{flex: 2}}>
        <View style={{flex: 1, flexDirection: 'row', padding: 24}}>
          <ScrollView>
            <Text style={{fontSize: 16, marginBottom: 14}}>Sinopsis</Text>
            <Text style={{fontSize: 14}}>{buku.sinopsis}</Text>
          </ScrollView>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  belakang: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  overlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: 'rgba(240,240,232,0.5)',
  },
});

export default Detail;
