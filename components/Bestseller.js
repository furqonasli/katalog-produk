import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';

const Bestseller = ({navigation}) => {
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch('https://datacoba-api.herokuapp.com/databestseller')
      .then(response => response.json())
      .then(hasil => setData(hasil))
      .catch(error => {
        console.log;
      });
  }, []);

  const listBuku = ({item}) => {
    return (
      <TouchableOpacity
        onPress={() => navigation.navigate('Detail', {buku: item})}>
        <View style={styles.listItem}>
          <Image source={{uri: item.image}} style={styles.coverImage} />
          <View style={{marginLeft: 5}}>
            <Text style={styles.title}>{item.judul}</Text>
            <Text style={styles.teks}>Harga: Rp. {item.harga}</Text>
            <Text style={styles.teks}>Penulis: {item.penulis}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <FlatList
        data={data}
        renderItem={listBuku}
        keyExtractor={item => item.id}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f8f8f8',
    alignItems: 'center',
  },
  listItem: {
    marginTop: 10,
    paddingVertical: 20,
    paddingHorizontal: 20,
    backgroundColor: '#fff',
    flexDirection: 'row',
  },
  coverImage: {
    width: 100,
    height: 150,
    borderRadius: 10,
  },
  teks: {
    fontSize: 14,
    width: 200,
    padding: 10,
  },
  title: {
    fontSize: 16,
    width: 220,
    fontWeight: 'bold',
    padding: 10,
    marginBottom: 15,
  },
});

export default Bestseller;
